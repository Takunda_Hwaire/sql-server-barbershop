from django import forms
from .models import Member, Appointment , Employee


class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = ["first_name", "last_name", "gender", "birth_date", "street", "city", "postal_code", "email"]
        

class MemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ["first_name","last_name", "gender", "birth_date", "street", "city", "postal_code", "email"]


class AppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        fields = ["first_name", "last_name","barber","email","date","time"]