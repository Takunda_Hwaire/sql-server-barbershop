from . import views
from django.urls import path
from .views import about

urlpatterns = [
    path('', views.index, name = "index"),
    path('about/',views.about,name = 'about'),
    path('contact/',views.contact,name = 'contact'),
    path('appointment/',views.appointment,name = 'appointment'),
    path('services/',views.services,name = 'services'),
    
    
   
]    