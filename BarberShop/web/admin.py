from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Employee ,Member , Product, Service , Appointment



@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "gender","birth_date","street","city","postal_code","email")

@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "gender","birth_date","street","city","postal_code","email")

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ("price","description","name")

@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ("service_type", "name", "price")
    

@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name","barber","email","date","time")    

