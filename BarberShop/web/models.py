from django.db import models


from django.db import models
from datetime import datetime, date

# Create your models here.


class Employee(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    gender = models.CharField(max_length=6)
    birth_date = models.DateField(auto_now_add=False
    ,auto_now=False,
    blank=True)
    street = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    postal_code = models.IntegerField()
    email = models.EmailField(default="")


    def __str__(self):
        return f" {self.first_name}, {self.last_name}, {self.gender},{self.birth_date},{self.street},{self.city}, {self.postal_code},{self.email}"
                 

                  
                  

class Member(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    gender = models.CharField(max_length=6)
    birth_date = models.DateField(auto_now_add=False
    ,auto_now=False,
    blank=True)
    street = models.CharField(max_length=30)
    city = models.CharField(max_length=30)
    postal_code = models.IntegerField()
    email = models.EmailField(default="")

    def __str__(self):
        return f"{self.first_name}, {self.last_name}, {self.gender},{self.birth_date},{self.street},{self.city},{self.postal_code},{self.email}"
                 

                 


class Product(models.Model):
    price = models.FloatField()
    description = models.CharField(max_length=20)
    name = models.CharField(max_length=20)


    def __str__(self):
        return f"{self.price}, {self.description},{self.name}"


class Service(models.Model):
    service_type = models.CharField(max_length=15)
    name = models.CharField(max_length=15)
    price = models.FloatField()

    def __str__(self):
        return f"{self.service_type}, {self.name},{self.price}"


class Appointment(models.Model):
     first_name = models.CharField(max_length=64)
     last_name = models.CharField(max_length=64)
     barber = models.CharField(max_length=64, blank=True, null=True)
     email = models.EmailField(max_length=254, blank=True, null=True)
     date = models.DateField(auto_now_add=False,blank=False)
     time = models.TimeField(auto_now_add=False, blank=False)

     def __str__(self):
         return f"{self.first_name}, {self.last_name},{self.barber},{self.email},{self.date},{self.time}"
     
  

